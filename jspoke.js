var DEBUG_MESSAGES = false;
var DEBUG_WINDOWING = false;

var cout = console.log.bind(console);
var lastButton;
var mainCanvas = null;

var pokeIds = {}

var keys = {
    0x50: "@",
    0x54: "#",
    0x75: "…",

    0x79: "┌",
    0x7A: "─",
    0x7B: "┐",
    0x7C: "│",
    0x7D: "└",
    0x7E: "┘",

    0x74: "№",

    0x7F: " ",
    0x80: "A",
    0x81: "B",
    0x82: "C",
    0x83: "D",
    0x84: "E",
    0x85: "F",
    0x86: "G",
    0x87: "H",
    0x88: "I",
    0x89: "J",
    0x8A: "K",
    0x8B: "L",
    0x8C: "M",
    0x8D: "N",
    0x8E: "O",
    0x8F: "P",
    0x90: "Q",
    0x91: "R",
    0x92: "S",
    0x93: "T",
    0x94: "U",
    0x95: "V",
    0x96: "W",
    0x97: "X",
    0x98: "Y",
    0x99: "Z",
    0x9A: "(",
    0x9B: ")",
    0x9C: ":",
    0x9D: ";",
    0x9E: "[",
    0x9F: "]",
    0xA0: "a",
    0xA1: "b",
    0xA2: "c",
    0xA3: "d",
    0xA4: "e",
    0xA5: "f",
    0xA6: "g",
    0xA7: "h",
    0xA8: "i",
    0xA9: "j",
    0xAA: "k",
    0xAB: "l",
    0xAC: "m",
    0xAD: "n",
    0xAE: "o",
    0xAF: "p",
    0xB0: "q",
    0xB1: "r",
    0xB2: "s",
    0xB3: "t",
    0xB4: "u",
    0xB5: "v",
    0xB6: "w",
    0xB7: "x",
    0xB8: "y",
    0xB9: "z",
    0xC0: "Ä",
    0xC1: "Ö",
    0xC2: "Ü",
    0xC3: "ä",
    0xC4: "ö",
    0xC5: "ü",
    0xD0: "'d",
    0xD1: "'l",
    0xD2: "'m",
    0xD3: "'r",
    0xD4: "'s",
    0xD5: "'t",
    0xD6: "'v",
    0xE0: "'",
    0xE3: "-",
    0xE6: "?",
    0xE7: "!",
    0xE8: ".",
    0xE9: "&",
    0xEA: "é",
    0xEB: "→",
    0xEC: "▷",
    0xED: "▶",
    0xEE: "▼",
    0xEF: "♂",
    0xF0: "¥",
    0xF1: "×",
    0xF3: "/",
    0xF4: ",",
    0xF5: "♀",
    0xF6: "0",
    0xF7: "1",
    0xF8: "2",
    0xF9: "3",
    0xFA: "4",
    0xFB: "5",
    0xFC: "6",
    0xFD: "7",
    0xFE: "8",
    0xFF: "9",
}

var pokedex = {
	0x01: "Rhydon",
	0x02: "Kangaskhan",
	0x03: "Nidoran",
	0x04: "Clefairy",
	0x05: "Spearow",
	0x06: "Voltorb",
	0x07: "Nidoking",
	0x08: "Slowbro",
	0x09: "Ivysaur",
	0x0A: "Exeggutor",
	0x0B: "Lickitung",
	0x0C: "Exeggcute",
	0x0D: "Grimer",
	0x0E: "Gengar",
	0x0F: "Nidoran",
	0x10: "Nidoqueen",
	0x11: "Cubone",
	0x12: "Rhyhorn",
	0x13: "Lapras",
	0x14: "Arcanine",
	0x15: "Mew",
	0x16: "Gyarados",
	0x17: "Shellder",
	0x18: "Tentacool",
	0x19: "Gastly",
	0x1A: "Scyther",
	0x1B: "Staryu",
	0x1C: "Blastoise",
	0x1D: "Pinsir",
	0x1E: "Tangela",
	0x21: "Growlithe",
	0x22: "Onix",
	0x23: "Fearow",
	0x24: "Pidgey",
	0x25: "Slowpoke",
	0x26: "Kadabra",
	0x27: "Graveler",
	0x28: "Chansey",
	0x29: "Machoke",
	0x2A: "Mr Mime",
	0x2B: "Hitmonlee",
	0x2C: "Hitmonchan",
	0x2D: "Arbok",
	0x2E: "Parasect",
	0x2F: "Psyduck",
	0x30: "Drowzee",
	0x31: "Golem",
	0x33: "Magmar",
	0x35: "Electabuzz",
	0x36: "Magneton",
	0x37: "Koffing",
	0x39: "Mankey",
	0x3A: "Seel",
	0x3B: "Diglett",
	0x3C: "Tauros",
	0x40: "Farfetch'd",
	0x41: "Veronat",
	0x42: "Dragonite",
	0x46: "Doduo",
	0x47: "Poliwag",
	0x48: "Jynx",
	0x49: "Moltres",
	0x4A: "Articuno",
	0x4B: "Zapdos",
	0x4C: "Ditto",
	0x4D: "Meowth",
	0x4E: "Krabby",
	0x52: "Vulpix",
	0x53: "Ninetales",
	0x54: "Pikachu",
	0x55: "Raichu",
	0x58: "Dratini",
	0x59: "Dragonair",
	0x5A: "Kabuto",
	0x5B: "Kabutops",
	0x5C: "Horsea",
	0x5D: "Seadra",
	0x60: "Sandshrew",
	0x61: "Sandslash",
	0x62: "Omanyte",
	0x63: "Omastar",
	0x64: "Jigglypuff",
	0x65: "Wiglytuff",
	0x66: "Eevee",
	0x67: "Flareon",
	0x68: "Jolteon",
	0x69: "Vaporeon",
	0x6A: "Machop",
	0x6B: "Zubat",
	0x6C: "Ekans",
	0x6D: "Paras",
	0x6E: "Poliwhirl",
	0x6F: "Poliwrath",
	0x70: "Weedle",
	0x71: "Kakuna",
	0x72: "Beedrill",
	0x74: "Dodrio",
	0x75: "Primeape",
	0x76: "Dugtrio",
	0x77: "Venomoth",
	0x78: "Dewgong",
	0x7B: "Caterpie",
	0x7C: "Metapod",
	0x7D: "Butterfree",
	0x7E: "Machamp",
	0x80: "Golduck",
	0x81: "Hypno",
	0x82: "Golbat",
	0x83: "Mewtwo",
	0x84: "Snorlax",
	0x85: "Magikarp",
	0x88: "Muk",
	0x8A: "Kingler",
	0x8B: "Cloyster",
	0x8D: "Electrode",
	0x8E: "Clefable",
	0x8F: "Weezing",
	0x90: "Persian",
	0x91: "Marowak",
	0x93: "Haunter",
	0x94: "Abra",
	0x95: "Alakazam",
	0x96: "Pidgeotto",
	0x97: "Pidgeot",
	0x98: "Starmie",
	0x99: "Bulbasaur",
	0x9A: "Venusaur",
	0x9B: "Tentacruel",
	0x9D: "Goldeen",
	0x9E: "Seaking",
	0xA3: "Ponyta",
	0xA4: "Rapidash",
	0xA5: "Rattata",
	0xA6: "Raticate",
	0xA7: "Nidorino",
	0xA8: "Nidorina",
	0xA9: "Geodude",
	0xAA: "Porygon",
	0xAB: "Aerodactyl",
	0xAD: "Magnemite",
	0xB0: "Charmander",
	0xB1: "Squirtle",
	0xB2: "Charmeleon",
	0xB3: "Wartortle",
	0xB4: "Charizard",
	0xB6: "Kabutops",
	0xB7: "Aerodactyl",
	0xB9: "Oddish",
	0xBA: "Gloom",
	0xBB: "Vileplume",
	0xBC: "Bellsprout",
	0xBD: "Weepinbell",
	0xBE: "Victreebel"
}


var maps = {
	0x00: "Pallet Town",
	0x01: "Viridian City",
	0x02: "Pewter City",
	0x03: "Cerulean City",
	0x04: "Lavender Town",
	0x05: "Vermilion City",
	0x06: "Celadon City",
	0x07: "Fuchsia City",
	0x08: "Cinnabar Island",
	0x09: "Indigo Plateau",
	0x0A: "Saffron City",
	0x0C: "Route 1",
	0x0D: "Route 2",
	0x0E: "Route 3",
	0x0F: "Route 4",
	0x10: "Route 5",
	0x11: "Route 6",
	0x12: "Route 7",
	0x13: "Route 8",
	0x14: "Route 9",
	0x15: "Route 10",
	0x16: "Route 11",
	0x17: "Route 12",
	0x18: "Route 13",
	0x19: "Route 14",
	0x1A: "Route 15",
	0x1B: "Route 16",
	0x1C: "Route 17",
	0x1D: "Route 18",
	0x1E: "Route 19",
	0x1F: "Route 20",
	0x20: "Route 21",
	0x21: "Route 22",
	0x22: "Route 23",
	0x23: "Route 24",
	0x24: "Route 25",
	0x25: "My House 1F",
	0x26: "My House 2F",
	0x27: "Rival's House",
	0x28: "Oak's Lab",
	0x29: "Viridian City Pokémon Center",
	0x2A: "Viridian City Poké Mart",
	0x2B: "Viridian City School",
	0x2C: "Viridian City House",
	0x2D: "Viridian Gym",
	0x2E: "Diglett's Cave Exit",
	0x2F: "Viridian Forest Exit",
	0x30: "Route 2 House",
	0x31: "Route 2 Gate",
	0x32: "Viridian Forest Entrance",
	0x33: "Viridian Forest",
	0x34: "Museum 1F",
	0x35: "Museum 2F",
	0x36: "Pewter Gym",
	0x37: "Pewter City House",
	0x38: "Pewter City Poké Mart",
	0x39: "Pewter City House",
	0x3A: "Pewter City Pokémon Center",
	0x3B: "Mt. Moon",
	0x3C: "Mt. Moon",
	0x3D: "Mt. Moon",
	0x3E: "Trashed House",
	0x3F: "Cerulean City House",
	0x40: "Cerulean City Pokémon Center",
	0x41: "Cerulean Gym",
	0x42: "Bike Shop",
	0x43: "Cerulean City Poké Mart",
	0x44: "Mt. Moon Pokémon Center",
	0x45: "Trashed House",
	0x46: "Route 5 Gate",
	0x47: "Route 5 Entrance",
	0x48: "Day Care",
	0x49: "Route 6 Gate",
	0x4A: "Route 6 Entrance",
	0x4B: "Route 6 Entrance",
	0x4C: "Route 7 Gate",
	0x4D: "Route 7 Entrance",
	0x4E: "Route 7 Entrance",
	0x4F: "Route 8 Gate",
	0x50: "Route 8 Entrance",
	0x51: "Rock Tunnel Pokémon Center",
	0x52: "Rock Tunnel",
	0x53: "Power Plant",
	0x54: "Route 11 Gate 1F",
	0x55: "Diglett's Cave Entrance",
	0x56: "Route 11 Gate 2F",
	0x57: "Route 12 Gate",
	0x58: "Bill's House",
	0x59: "Vermilion City Pokémon Center",
	0x5A: "Pokémon Fan Club",
	0x5B: "Vermilion City Poké Mart",
	0x5C: "Vermilion Gym",
	0x5D: "Vermilion City House",
	0x5E: "Vermilion City Dock",
	0x5F: "SS Anne",
	0x60: "SS Anne",
	0x61: "SS Anne",
	0x62: "SS Anne",
	0x63: "SS Anne",
	0x64: "SS Anne",
	0x65: "SS Anne",
	0x66: "SS Anne",
	0x67: "SS Anne",
	0x68: "SS Anne",
	0x6C: "Victory Road",
	0x71: "Lance's Room",
	0x76: "Hall of Fame",
	0x77: "Underground Path",
	0x78: "Champion's Room",
	0x79: "Underground Path",
	0x7A: "Celadon Department Store",
	0x7B: "Celadon Department Store",
	0x7C: "Celadon Department Store",
	0x7D: "Celadon Department Store",
	0x7E: "Celadon Department Store Roof",
	0x7F: "Celadon Department Store Elevator",
	0x80: "Celadon Mansion",
	0x81: "Celadon Mansion",
	0x82: "Celadon Mansion",
	0x83: "Celadon Mansion",
	0x84: "Celadon Mansion",
	0x85: "Celadon Pokémon Center",
	0x86: "Celadon Gym",
	0x87: "Game Corner",
	0x88: "Celadon Department Store",
	0x89: "Celadon Prize Room",
	0x8A: "Celadon City Diner",
	0x8B: "Celadon City House",
	0x8C: "Celadon Hotel",
	0x8D: "Lavender Pokémon Center",
	0x8E: "Pokémon Tower",
	0x8F: "Pokémon Tower",
	0x90: "Pokémon Tower",
	0x91: "Pokémon Tower",
	0x92: "Pokémon Tower",
	0x93: "Pokémon Tower",
	0x94: "Pokémon Tower",
	0x95: "Lavender City Pokémon Center",
	0x96: "Lavender City Poké Mart",
	0x97: "Lavender City House",
	0x98: "Fuchsia City Poké Mart",
	0x99: "Fuchsia City House",
	0x9A: "Fuchsia City Pokémon Center",
	0x9B: "Fuchsia City House",
	0x9C: "Safari Zone Entrance",
	0x9D: "Fuchsia Gym",
	0x9E: "Fuchsia Meeting Room",
	0x9F: "Seafoam Islands",
	0xA0: "Seafoam Islands",
	0xA1: "Seafoam Islands",
	0xA2: "Seafoam Islands",
	0xA3: "Vermilion City House",
	0xA4: "Fuchsia City House",
	0xA5: "Mansion",
	0xA6: "Cinnabar Gym",
	0xA7: "Cinnabar Lab",
	0xA8: "Cinnabar Lab",
	0xA9: "Cinnabar Lab",
	0xAA: "Cinnabar Lab",
	0xAB: "Cinnabar City Pokémon Center",
	0xAC: "Cinnabar City Poké Mart",
	0xAD: "Cinnabar City Poké Mart",
	0xAE: "Indigo Plateau Lobby",
	0xAF: "Copycat's House 1F",
	0xB0: "Copycat's House 2F",
	0xB1: "Fighting Dojo",
	0xB2: "Saffron Gym",
	0xB3: "Saffron City House",
	0xB4: "Saffron City Poké Mart",
	0xB5: "Silph Co. 1F",
	0xB6: "Saffron City Pokémon Center",
	0xB7: "Saffron City House",
	0xB8: "Route 15 Gate 1F",
	0xB9: "Route 15 Gate 2F",
	0xBA: "Route 16 Gate 1F",
	0xBB: "Route 16 Gate 2F",
	0xBC: "Route 16 House",
	0xBD: "Route 12 House",
	0xBE: "Route 18 Gate 1F",
	0xBF: "Route 18 Gate 2F",
	0xC0: "Seafoam Islands",
	0xC1: "Route 22 Gate",
	0xC2: "Victory Road",
	0xC3: "Route 12 Gate 2F",
	0xC4: "Vermilion City House",
	0xC5: "Diglett's Cave",
	0xC6: "Victory Road",
	0xC7: "Rocket Hideout",
	0xC8: "Rocket Hideout",
	0xC9: "Rocket Hideout",
	0xCA: "Rocket Hideout",
	0xCB: "Rocket Hideout Elevator",
	0xCF: "Silph Co. 2F",
	0xD0: "Silph Co. 3F",
	0xD1: "Silph Co. 4F",
	0xD2: "Silph Co. 5F",
	0xD3: "Silph Co. 6F",
	0xD4: "Silph Co. 7F",
	0xD5: "Silph Co. 8F",
	0xD6: "Mansion",
	0xD7: "Mansion",
	0xD8: "Mansion",
	0xD9: "Safari Zone East",
	0xDA: "Safari Zone North",
	0xDB: "Safari Zone West",
	0xDC: "Safari Zone Center",
	0xDD: "Safari Zone Rest House",
	0xDE: "Safari Zone Secret House",
	0xDF: "Safari Zone Rest House",
	0xE0: "Safari Zone Rest House",
	0xE1: "Safari Zone Rest House",
	0xE2: "78Y97XEA1XUQADR8YM59",
	0xE3: "X7RPWWUEADZ2RHHTFMW6",
	0xE4: "VCEL2JEQMLZMHM57ZFH8",
	0xE5: "Name Rater's House",
	0xE6: "Cerulean City House",
	0xE8: "Rock Tunnel",
	0xE9: "Silph Co. 9F",
	0xEA: "Silph Co. 10F",
	0xEB: "Silph Co. 11F",
	0xEC: "Silph Co. Elevator",
	0xEF: "Battle Center",
	0xF0: "Trade Center",
	0xF5: "Lorelei's Room",
	0xF6: "Bruno's Room",
	0xF7: "Agatha's Room"
}

var haveDls = {}

var isProgressiveGoal;

var mapBounds;
var mapRect;

var trainerPos;
var lastPos;
var goal;
var MAP_EXTENSION = 1;
var regex = / [a-z]+ /i
var textOn = false;

var visitedGrid;
var mapName;
var lastMapName;
var pathfind;
var path;
var finder;

var boundingBox;
var boundOrigin;
var dirWeights;
var lastBox;

var intermediate;
var goIntermediate = false;
var finalGoal;

// in ExtendGoal, how many times can we fail to extend the bounding box before setting a random goal
var defaultFailTolerance = 5;
var boundFailTolerance = 5;
var boundFails;

// for NoBack
opposite = {"right":"left", "left":"right", "up":"down", "down":"up"}

//friendly_maps = {1:"Left",2:"Right",3:"Down",4:"Up",5:"A",6:"B",7:"Start"}
probs = [0.14,0.145,0.186,0.156,0.149,0.144,0.08]
speed = 3;
var keymap = ["right", "left", "up", "down", "a", "b", "start"];	//Keyboard button map.

var buttonInterval = 300/speed
stuckCount = 0;
stuckLimit = 10;


keyDownTime = 100;
goalChance = 0.9 // chance of trying to pursue goal

/*
NoBack: random walk but can't go in the direction it *just* came from
RandomGoal: picks a point on the map and tries to walk to it
ProgressiveGoal: picks a random goal on the map where notrainer hasn't been
ExtendGoal: picks goals in the direction most-travelled
*/
var strategies = {"NoBack":1,"RandomGoal":2,"ProgressiveGoal":3,"ExtendGoal":4}
var strategy = strategies.ExtendGoal

var setSpeed = false;

var waitKeyDown;
var waitKeyUp;

window.onload = function () {
    windowingInitialize();
}

var generateWeighedList = function(list, weight) {
    var weighed_list = [];

    // Loop over weights
    for (var i = 0; i < weight.length; i++) {
        var multiples = weight[i] * 100;

        // Loop over the list of items
        for (var j = 0; j < multiples; j++) {
            weighed_list.push(list[i]);
        }
    }

    return weighed_list;
};
var weightedKeys;
var metaButtons = ["a","b","start"]

function loadGame() {
  var stateName = "FREEZE_POKEMON BLU_0"
  openState(stateName,mainCanvas)
}

function windowingInitialize() {
	cout("windowingInitialize() called.", 0);
  //pickRandomColor();
	mainCanvas = document.getElementById("mainCanvas");
  //registerTouchEventShim();
  window.onunload = autoSave;
  ("MozActivity" in window ? loadViaMozActivity : loadViaXHR)();
}
var DEBUG_MESSAGES = false;
var DEBUG_WINDOWING = false;
window.addEventListener("DOMContentLoaded", windowingInitialize);

function loadViaXHR () {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "pokeblue.gb");
  xhr.responseType = "blob";
  xhr.onload = function () {
    startGame(new Blob([this.response], { type: "text/plain" }));
  };
  xhr.send();
};


function setRandomKey() {
  	if (GameBoyEmulatorInitialized()) {
      gameboy.setSpeed(speed);
      trainerPos = {"x":gameboy.memoryRead(0xd362)+(MAP_EXTENSION), "y":gameboy.memoryRead(0xd361)+(MAP_EXTENSION)}
    }




    if(strategy == strategies.NoBack) {
      // don't let me go back in the previous direction
      if (lastButton == null || !(lastButton in opposite)) {
        lastButton = weightedKeys[Math.floor(Math.random() * weightedKeys.length)];
      }
      else {
        nextButton = opposite[lastButton];
        //console.log("next:" + nextButton)
        while(nextButton == opposite[lastButton] || !(nextButton in opposite)) {

          nextButton = weightedKeys[Math.floor(Math.random() * weightedKeys.length)];
          if(nextButton != opposite[lastButton] || !(nextButton in opposite)) {
            break;
          }
        }
        lastButton = nextButton;
      }
    }

		else if(textOn == false && (strategy == strategies.RandomGoal || strategy == strategies.ProgressiveGoal || strategy == strategies.ExtendGoal) && trainerPos != null && goal != null) {

			if(Math.random() > goalChance) {
				lastButton = weightedKeys[Math.floor(Math.random() * weightedKeys.length)];
        //lastButton = metaButtons[Math.floor(Math.random() * metaButtons.length)];

				return;
			}

      if(path == null || path.length == 0) {

  			poss_directions = []
  			if(trainerPos.x < goal.x)
  				poss_directions.push("right")
  			if(trainerPos.x > goal.x)
  				poss_directions.push("left")
  			if(trainerPos.y < goal.y)
  				poss_directions.push("down")
  			if(trainerPos.y > goal.y)
  				poss_directions.push("up")

  			//console.log(poss_directions)
  			//console.log(trainerPos)
  			lastButton = poss_directions[Math.floor(Math.random() * poss_directions.length)]

  			if(poss_directions.length == 0)
  				  lastButton = weightedKeys[Math.floor(Math.random() * weightedKeys.length)];
            //lastButton = metaButtons[Math.floor(Math.random() * metaButtons.length)];
      }
      // if we have a path, go to the next step
      else {
        poss_directions = []
        trainerPos = {"x":gameboy.memoryRead(0xd362)+(MAP_EXTENSION), "y":gameboy.memoryRead(0xd361)+(MAP_EXTENSION)}


        //if(path[0][0] == trainerPos.x && path[0][1] == trainerPos.y)
        //posOnPath = path.indexOf([trainerPos.x,trainerPos.y])
        posOnPath = -1
        for(var i=0; i<path.length; i++) {
          if(path[i][0] == trainerPos.x && path[i][1] == trainerPos.y) {
            posOnPath = i;
          }
        }

        if(posOnPath > -1)
        {
          path.splice(0,(posOnPath+1))
        }

        if(path.length == 0)
          return;
        if(path[0][0] < trainerPos.x)
          poss_directions.push("left")
        if(path[0][0] > trainerPos.x)
          poss_directions.push("right")
        if(path[0][1] < trainerPos.y)
          poss_directions.push("up")
        if(path[0][1] > trainerPos.y)
          poss_directions.push("down")

        //console.log(path)
        //console.log(trainerPos.x +"," + trainerPos.y + ": going " + poss_directions[0] + " for " + path[0])
        lastButton = poss_directions[Math.floor(Math.random() * poss_directions.length)]

      }

		}

    else {
      lastButton = weightedKeys[Math.floor(Math.random() * weightedKeys.length)];
    }

    //console.log("key:" + lastButton)
}

function getText() {
	bounds = 2000
	memory = ""
	start = 0xc103
	text = ""
	for(i=start; i<(start+bounds); i++)
	{
		this_mem =  gameboy.memoryRead(i)
		memory = memory + this_mem;
		if(this_mem in keys)
			text += keys[this_mem]
	}
	text_re = text.match(regex)
	//console.log("text:" + text_re)

	if(text_re == null) {
		//$(".textOn").hide();
		textOn = false;
	}

	else {
		//$(".textOn").show();
		textOn = true;
		stuckCount = 0;
	}

}

function setMapGrid(x,y) {
	visitedGrid = new Array(y);
	for (var i = 0; i < y; i++) {
	  visitedGrid[i] = new Array(x);
	}
}

function rndInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray);
}

function getPathfindGrid() {
  if(mapBounds.x + mapBounds.y <= 4) {
    return;
  }
  pathfind = new PF.Grid(mapBounds.x, mapBounds.y)
  for(y=0; y<mapBounds.y; y++) {
    for(x=0; x<mapBounds.x; x++) {
      pathfind.setWalkableAt(x,y,false);
      if(y in visitedGrid && visitedGrid[y][x] == 1) {
        pathfind.setWalkableAt(x,y,true);
      }
    }
  }
  // add goal to pathfind
  try{
    pathfind.setWalkableAt(goal.x,goal.y,true);
  }
  catch(e) {
    console.log("Pathfind error")
  }

  finder = new PF.AStarFinder()
  path = finder.findPath(trainerPos.x,trainerPos.y,goal.x,goal.y,pathfind)
  //console.log("Path to goal: " + path)

  //if(path.length == 0) {
  if(path.length == 0 && !goIntermediate) {
    //  if can't pathfind, pick random points away from goal until intermediate found

    extendRect = new paper.Rectangle(new paper.Point(goal.x,goal.y),new paper.Size(1,1));
    do {
      extendDir = rndInclusive(1,4)

      if(extendDir == 1) {
        point = rndInclusive(extendRect.left, extendRect.right)
        extendPoint = new paper.Point(point,(extendRect.top-1))
      }
      else if (extendDir == 2) {
        point = rndInclusive(extendRect.top, extendRect.bottom)
        extendPoint = new paper.Point((extendRect.right+1),point)
      }
      else if(extendDir == 3) {
        point = rndInclusive(extendRect.left, extendRect.right)
        extendPoint = new paper.Point(point,(extendRect.bottom+1))
      }
      else if (extendDir == 4) {
        point = rndInclusive(extendRect.top, extendRect.bottom)
        extendPoint = new paper.Point((extendRect.left-1),point)
      }

      extendRect = extendRect.include(extendPoint)
      if(extendPoint.y in visitedGrid && visitedGrid[extendPoint.y][extendPoint.x]) {
        thisRow = $("tr:eq("+(goal.y)+")")
        thisCell = thisRow.find("td:eq("+(goal.x)+")");
        thisCell.addClass("intermediate");
        intermediate = extendPoint;
        finalGoal = {x:goal.x, y:goal.y}
        goal = {x:extendPoint.x, y:extendPoint.y};

        console.log("trying to get to {"+finalGoal.x+","+finalGoal.y+"} via {"+goal.x+","+goal.y+"}")
        goIntermediate = true;
        break;

      }
      else {
        thisRow = $("tr:eq("+(extendPoint.y)+")")
        thisCell = thisRow.find("td:eq("+(extendPoint.x)+")");
        //thisCell.addClass("noreach");
      }
      if((extendRect.x * extendRect.y) > (mapBounds.x * mapBounds.y)) {
        //failed to find an intersection
        return;
      }
    }
    //while((extendRect.x * extendRect.y) < (mapBounds.x * mapBounds.y))
    while(mapRect.contains(extendPoint));

    if(goIntermediate)
      getPathfindGrid();
  }

}

function turnPump() {
  // handle map change and battle

	if(!(GameBoyEmulatorInitialized()))
		return;

  theseMapBounds = {"x":(gameboy.memoryRead(0xd369)*2)+(MAP_EXTENSION*2),"y":(gameboy.memoryRead(0xd368)*2)+(MAP_EXTENSION*2)}
  trainerPos = {"x":gameboy.memoryRead(0xd362)+(MAP_EXTENSION), "y":gameboy.memoryRead(0xd361)+(MAP_EXTENSION)}


	if(textOn == false && lastPos != null && ((trainerPos.x == lastPos.x) && (trainerPos.y == lastPos.y))) {
		stuckCount += 1
	}
	else {
		stuckCount = 0
	}

	if (goal != null && (trainerPos.x == goal.x && trainerPos.y == goal.y)) {
		goal = null;
    $("td").removeClass("goal");
    $("td").removeClass("intermediate");
    $("td").removeClass("noreach");

    if(finalGoal != null) {
      goal = {"x":finalGoal.x, "y":finalGoal.y}
      finalGoal = null;
      goalRow = $("tr:eq("+goal.y+")")
      goalCell = goalRow.find("td:eq("+goal.x+")");
      goalCell.addClass("goal")
    }
		console.log("Goal achieved!")
    //boundFailTolerance += 1
    //boundFails = 0;

    finalGoal = null;
    goIntermediate = false;

    //stuckLimit += 1
    $("#stuck_limit").text(stuckLimit);
	}

	if(stuckCount >= stuckLimit) {
		goal = null;
		console.log("Stuck, picking new goal")
		$("td").removeClass("goal");
    $("td").removeClass("intermediate");
    $("td").removeClass("noreach");
    finalGoal = null;
    goIntermediate = false;

		stuckCount = 0;
    //boundFailTolerance -= 1
    if(boundFailTolerance <= 1) {
      boundFailTolerance = 2;
    }

    //stuckLimit -= 1
    $("#stuck_limit").text(stuckLimit);
    if(stuckLimit < 2) {
      //stuckLimit = 10
    }
	}

  mapName = maps[gameboy.memoryRead(0xd35e)]

  //if(mapBounds == null || (theseMapBounds.x != mapBounds.x || theseMapBounds.y != mapBounds.y)) {
  if(mapBounds == null || (mapName != lastMapName) || (theseMapBounds.x != mapBounds.x || theseMapBounds.y != mapBounds.y)) {
    // map change
		setMapGrid(theseMapBounds.x, theseMapBounds.y);
    console.log("New map!")
    boundFailTolerance = defaultFailTolerance;
    boundingBox = new paper.Rectangle(new paper.Point(trainerPos.x,trainerPos.y),new paper.Size(1,1));
    boundOrigin = {"x":trainerPos.x,"y":trainerPos.y}

		goal = null;
		$("td").removeClass("goal");
    $("td").removeClass("intermediate");
    $("td").removeClass("noreach");
    finalGoal = null;
    goIntermediate = false;

    $("table").empty();
    $("#map_bounds").text("x: " + theseMapBounds.x + " y: " + theseMapBounds.y);

    rowString = "";
    for(x=0; x<theseMapBounds.x; x++) {
      if(x < MAP_EXTENSION || x >= (theseMapBounds.x-MAP_EXTENSION)) {
        rowString += "<td class='outBounds'></td>"
      }
      else {
        rowString += "<td></td>"
      }
    }
    for(y=0; y<theseMapBounds.y; y++) {
      $("table").append("<tr>"+rowString+"</tr>")
    }
  }

  $("tr:lt("+MAP_EXTENSION+")").find("td").addClass("outBounds");
  $("tr:gt("+(theseMapBounds.y-(MAP_EXTENSION+1))+")").find("td").addClass("outBounds");

  $("td").removeClass("trainer");

  thisRow = $("tr:eq("+(trainerPos.y)+")")
  thisCell = thisRow.find("td:eq("+(trainerPos.x)+")");
  thisCell.addClass("trainer");
  mapBounds = theseMapBounds;
  mapRect = new paper.Rectangle(0,0,mapBounds.x,mapBounds.y);

	// paint current location as visited
  if(visitedGrid.length != 0 && mapBounds.x > MAP_EXTENSION*2) {
	   visitedGrid[trainerPos.y][trainerPos.x] = 1
     boundingBox = boundingBox.include(new paper.Point(trainerPos.x,trainerPos.y));
	    thisCell.addClass("visited")
  }

  // set new goal
  needRandomGoal = false;
  isProgressiveGoal = true;
  if(strategy == strategies.ExtendGoal && goal == null) {
    if(lastBox != boundingBox.area)
      boundFails = 0;

    if(lastBox != null && lastBox == boundingBox.area) {
      boundFails += 1
      console.log(boundFails +"/" + boundFailTolerance + " until set random goal")
      if(boundFails >= boundFailTolerance) {
        needRandomGoal = true;
        isProgressiveGoal = false;
        console.log("Not extending bounding box, so setting random goal")

      }
    }
    lastBox = boundingBox.area;
  }

	if(needRandomGoal == true || (strategy == strategies.RandomGoal || strategy == strategies.ProgressiveGoal)) {
		if(goal == null) {
			do {
				x = Math.floor(Math.random() * (mapBounds.x))
				y = Math.floor(Math.random() * (mapBounds.y))
			}
			while(strategy == strategies.ProgressiveGoal && ((y in visitedGrid) && (visitedGrid[y][x] == 1)));



			console.log("New goal at x: " + x + " y: " + y)
			goalRow = $("tr:eq("+y+")")
			goalCell = goalRow.find("td:eq("+x+")");
			goalCell.addClass("goal")
			goal = {"x":x,"y":y}

      getPathfindGrid();
		}
	}

  else if(strategy == strategies.ExtendGoal) {
    if(goal == null) {

      // pick a goal on the edge of the bounding box in the direction we've travelled in most
      north = Math.abs(boundOrigin.y - boundingBox.top)
      east = Math.abs(boundOrigin.x - boundingBox.right)
      south = Math.abs(boundOrigin.y - boundingBox.bottom)
      west = Math.abs(boundOrigin.x - boundingBox.left)
      dirWeights = [north,east,south,west]
      nextWeight = getMaxOfArray(dirWeights)
      if(dirWeights.indexOf(nextWeight) == 0){
        go = rndInclusive(boundingBox.left,boundingBox.right)
        goal = {"x":go,"y":boundingBox.top-1}
      }
      else if(dirWeights.indexOf(nextWeight) == 1){
        go = rndInclusive(boundingBox.top,boundingBox.bottom)
        goal = {"x":boundingBox.right+1,"y":go}
      }
      else if(dirWeights.indexOf(nextWeight) == 2){
        go = rndInclusive(boundingBox.left,boundingBox.right)
        goal = {"x":go,"y":boundingBox.bottom+1}
      }
      else if(dirWeights.indexOf(nextWeight) == 3){
        go = rndInclusive(boundingBox.top,boundingBox.bottom)
        goal = {"x":boundingBox.left-1,"y":go}
      }

      if(boundingBox.width == boundingBox.height) {
        // if no dominant direction pick random goal
        do {
  				x = Math.floor(Math.random() * (mapBounds.x))
  				y = Math.floor(Math.random() * (mapBounds.y))
  			}
  			while(((y in visitedGrid) && (visitedGrid[y][x] == 1)));
        console.log("No dominant direction, picking random goal.")
        goal = {"x":x,"y":y}
      }

      // pathfind to goal
      getPathfindGrid();

      console.log("New goal at x: " + goal.x + " y: " + goal.y)
      goalRow = $("tr:eq("+goal.y+")")
      goalCell = goalRow.find("td:eq("+goal.x+")");
      goalCell.addClass("goal")
    }
  }

	lastPos = trainerPos;
  lastMapName = mapName;

}

function doPressUp() {
  clearTimeout(waitKeyUp);
  clearTimeout(waitKeyDown);

  GameBoyKeyUp(lastButton);
  setRandomKey();

  //turnPump();

  waitKeyDown = setTimeout(doPress,buttonInterval)
  //console.log("DOWN TIMER")
}

function doPress() {
  // randomly do some housekeeping
  if(Math.floor(Math.random() * 1000) == 500) {
    autoSave();
  }
  if(Math.floor(Math.random() * 100) == 50) {
    //stateUpdate();

  }
  turnPump();
  stateUpdate();
  GameBoyKeyDown(lastButton);
    //setRandomKey();
  waitKeyUp = setTimeout(doPressUp,keyDownTime/speed);
}

function getMapName() {
  mapId = gameboy.memoryRead(0xd35e)
  document.getElementById("map_name").innerHTML = maps[mapId]
}

function getBattleStatus() {
  isBattle = gameboy.memoryRead(0xd057)
  if(isBattle == 1) {
		stuckCount = 0
    level = gameboy.memoryRead(0xcff3)
    poke = pokedex[gameboy.memoryRead(0xcfe5)]
    // document.getElementById("battle").style.display = 'block';
    // document.getElementById("battle_name").innerHTML = "Fighting a level " + level + " " + poke
		// $("body").addClass("battle-mode")
  }
  // else if (isBattle == 0) {
  //   document.getElementById("battle").style.display = 'none';
	// 	$("body").removeClass("battle-mode")
  // }

}

function stateUpdate() {
	getText();
  getMapName();
  getBattleStatus();
  getTrainerStatus()
  getPokemonStatus()
}

var handlePokemonDL = function(data) {

}

function raiseNews() {
  headline = "CAUGHT A NEW POKÉMON"
  content = {"text":"CHARMANDER","level":20,"id":1}
}

//update state of current pokemon
function getPokemonStatus() {
  $(".pokemon").removeClass("no-pokemon")
  $(".hp").removeClass("critical")
  pokeNo = 0
  for(ram=0xD16B;ram<0xD248;ram+=0x2c) {
    pokeid = gameboy.memoryRead(ram)
    if(pokedex[pokeid] == null) {
      $(".pokemon:eq("+pokeNo+")").addClass("no-pokemon")
    }

    if((pokeid > 0) && ((pokeid in pokedex) && !(pokeid in pokeIds) && !(pokeid in haveDls))) {
      haveDls[pokeid] = 1
      pokeElement = $(".pokemon:eq("+pokeNo+")")
      pokeElement.find(".pokename").text(pokedex[pokeid])
      pokeElement.addClass("no-pokemon")
      $.ajax({
        url:"http://pokeapi.co/api/v2/pokemon/"+pokedex[pokeid].toLowerCase(),
        dataType:"json",
        context:{"pokeNo":pokeNo,"pokeid":pokeid}
      })
      .done(function(data) {
        pokeno = this.pokeNo
        pid = this.pokeid
        pokeElement = $(".pokemon:eq("+pokeno+")")
        pokeIds[pid] = data['id']
        sprite = data['sprites']['front_default']
        pokeElement.find("img").attr("src",sprite)
        pokeElement.removeClass("no-pokemon")
        pokeElement.find(".levelno").text(gameboy.memoryRead(ram+33));

        // get hp
        maxHp = gameboy.memoryRead(ram+0x23)
        hp = gameboy.memoryRead(ram+0x2)
        fill = (hp/maxHp)*100
        pokeElement.find(".hp").css("width",fill+"%")
        if(hp == 0) {
          pokeElement.find(".pokename").text(pokedex[pokeid]+ " (Fainted)")
        }


        // nickRam = 0xD25B + (10*pokeno)
        // nickname = ""
        // for(i=0; i<10; i++) {
        //   nickname += keys[gameboy.memoryRead[nickRam+i]]
        // }
        // pokeElement.find(".pokename").text(nickname)

      });

    }
    else {
      pid = pokeIds[pokeid];
      if(pid) {

        pokeElement = $(".pokemon:eq("+pokeNo+")")
        pokeElement.removeClass("no-pokemon")
        sprite = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pid+".png";
        pokeElement.find("img").attr("src",sprite)
        pokeElement.find(".levelno").text(gameboy.memoryRead(ram+33));
        // nickRam = 0xD25B + (10*pokeno)
        // nickname = ""
        // for(i=0; i<10; i++) {
        //   nickname += keys[gameboy.memoryRead[nickRam+i]]
        // }
        // pokeElement.find(".pokename").text(nickname)
        pokeElement.find(".pokename").text(pokedex[pokeid])

        // get hp
        maxHp = gameboy.memoryRead(ram+0x23)
        hp = gameboy.memoryRead(ram+0x2)
        fill = (hp/maxHp)*100
        if(fill < 20)
          pokeElement.find(".hp").addClass("critical")
        pokeElement.find(".hp").css("width",fill+"%")

        if(hp == 0) {
          pokeElement.find(".pokename").text(pokedex[pokeid]+ " (Fainted)")
        }

      }

    }
    pokeNo += 1;



  }

}

function getTrainerStatus() {
  trainer = ""
  for(i=0xD158; i<0xD163; i++) {
    trainer += keys[gameboy.memoryRead(i)]
  }
  if(!trainer.includes("@"))
    trainer = "Welcome to JS Plays Pokémon"
  else
    trainer = trainer.split("@")[0]

  if(trainer == "NINTEN")
    trainer = "Welcome to JS Plays Pokémon"

  $(".trainer_name").text(trainer)
}

function writeRandomMemory() {
  address = rndInclusive(0,65535);
  val = rndInclusive(0,255);
  console.log("writing " + val + " to " + address.toString(16));
  gameboy.memoryWrite(address,val)
}


function startGame (blob) {
  var binaryHandle = new FileReader();
  binaryHandle.onload = function () {
    if (this.readyState === 2) {
      try {
        start(mainCanvas, this.result);
      } catch (e) {
        alert(e.message);
      }
    }
  };
  binaryHandle.readAsBinaryString(blob);
  //loadGame();

  settings[3] = 0;

  //weightedKeys = generateWeighedList(keymap,probs)
  weightedKeys = keymap;

  setRandomKey();
  waitKeyDown = setTimeout(doPress,buttonInterval);
};
